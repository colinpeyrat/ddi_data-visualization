/*
 * TODO: Placer les bonhommes en absolu dans les colonnes et même faire un nivelement par rapport a la colonne a coté
 **/


/*
 * Age chart Module
 **/

var ageChart = (function () {



    /*
     * Object Navigation
     **/

    var Navigation = {
        el: null,

        init: function () {
            this.el = document.querySelector('#vizAgeNavigation');
            this.$item = this.el.querySelectorAll('.viz-age-nav__item');

            var item;
            for (var i = 0; i < this.$item.length; i++) {
                item = this.$item[i];
                this.$item[i].setAttribute('data-index', i);
                item.addEventListener('click', this.sendNavChanged.bind(this, i));
            }
        },

        toggleNav: function (item) {
            for (var i = 0; i < this.$item.length; i++) {
                this.$item[i].querySelector('a').classList.remove('is-active');
            }
            item.classList.add('is-active');
        },

        sendNavChanged: function (index, e) {
            e.preventDefault();
            var item = e.srcElement;

            this.toggleNav(item);

            this.el.dispatchEvent(new CustomEvent('navigationChanged', {detail: {index: index}}));
        }
    };


    /*
     * Object Person Chart
     **/

    var Person = function () {
        this.el = null;
        this.w = 10;
        this.h = 20;
        this.hidden = null;
    };

    Person.prototype.show = function () {
        TweenMax.set(this.el, {autoAlpha: 1});
        this.hidden = false;
    };
    Person.prototype.hide = function () {
        TweenMax.set(this.el, {autoAlpha: 0});
        this.hidden = true;
    };

    Person.prototype.init = function (id) {
        this.el = document.createElement('div');
        this.el.classList.add('viz-age-person');
        this.el.classList.add('is-' + id);
        this.hide();
    };


    /*
     * Object Column Chart
     **/
    var Column = function () {
        this.id = null;
        this.el = null;
        this.x = null;
        this.y = null;
        this.data = null;
        this.persons = [];
        this.maxX = document.querySelector('#datavizAge').offsetWidth;
        this.isAnimating = false;
    };

    Column.prototype.init = function (el, id) {
        this.id = id;
        this.el = el;
        this.x = this.el.offsetLeft + 5;
        this.y = this.el.offsetTop + this.el.offsetHeight;
    };

    Column.prototype.setPerson = function (datas) {
        var data;
        for (var i = 0; i < datas.length; i++) {
            data = datas[i];
        }
    };

    Column.prototype.render = function (index) {
        if (!this.isAnimating) {
            this.isAnimating = true;
            var data = this.data[index];
            var margin = {x: 10, y: 1};
            var person;
            var offsetX;
            var offsetY = this.y;
            var line = 0;

            this.hideAllPersons();

            for (var i = 0; i < data; i++) {
                person = this.persons[i];

                if (i % 5 == 0) {
                    offsetY -= 20 + margin.y;
                    line = 0;
                } else {
                    line++;
                    $
                }

                offsetX = (line * person.w);

                person.show();

                TweenMax.to(person.el, 0.5, {
                    delay: i * 0.0025,
                    y: offsetY,
                    x: this.x + offsetX,
                    force3D: true,
                    ease: Sine.easeOut,
                    onComplete: function () {
                        this.isAnimating = false;
                    },
                    onCompleteScope: this
                });
            }
        }
    };

    Column.prototype.hideAllPersons = function () {
        var _this = this;

        var loop = function loop() {
            var person = _this.persons[i];
            if (!person.hidden) {
                TweenMax.to(person.el, 0.5, {
                    delay: i * 0.0025,
                    y: 0,
                    x: _this.maxX,
                    force3D: true,
                    onComplete: function onComplete() {
                        person.hide();
                    }
                });
            }
        };

        for (var i = 0; i < this.persons.length; i++) {
            loop();
        }
    };

    /*
     * Object Chart
     **/
    var Chart = {
        el: document.querySelector('#datavizAge'),
        $persons: document.querySelector('#vizAgePersons'),
        columns: [],
        datas: [],
        navigation: null,
        current: 0,

        init: function () {
            this.navigation = Navigation;
            this.navigation.init();

            this.navigation.el.addEventListener('navigationChanged', this.switchData.bind(this));

            var columns = this.el.querySelectorAll('.viz-age-col__content .inner');
            var column;
            for (var i = 0; i < columns.length; i++) {
                column = new Column();
                column.init(columns[i], i);
                this.columns.push(column);
            }

            this.load();
            this.createPersons();
        },

        switchData: function (e) {
            this.current = e.detail.index;
            this.render();
        },

        render: function () {
            var column;
            for (var i = 0; i < this.columns.length; i++) {
                column = this.columns[i];
                column.render(this.current);
            }
        },

        createPersons: function () {
            var column;
            var persons;
            var person;
            var i;
            var j;
            for (i = 0; i < this.columns.length; i++) {
                column = this.columns[i];

                persons = [];

                for (j = 0; j < 100; j++) {
                    person = new Person();
                    person.init(column.id);

                    persons.push(person);

                    this.$persons.appendChild(person.el);
                }
                column.persons = persons;
            }
        },

        load: function () {
            this.req = new XMLHttpRequest();
            this.req.open('GET', 'data/age.json', true);
            this.req.addEventListener('readystatechange', this.loaded.bind(this));
            this.req.send();
        },

        loaded: function () {
            if (this.req.readyState == 4) {
                if (this.req.status == 200 || this.req.status == 304) {
                    var datas = JSON.parse(this.req.responseText);

                    for (var i = 0; i < datas.length; i++) {
                        this.columns[i].data = datas[i].value;
                    }
                }
            }
        }
    };

    return Chart;

})();


ageChart.init();

console.log(ageChart);
