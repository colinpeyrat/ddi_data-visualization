var userChart = (function () {
    var Graph = {
        ctx: null,
        graph: null,
        gradient1: null,
        gradient2: null,
        datas: null,
        isBuilded: null,


        buildChart: function () {

            if (!this.isBuilded) {
                this.isBuilded = true;
                this.chart = new Chart(this.ctx, {

                    type: 'bar',
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        scaleOverride: true,
                        scaleStartValue: 0,

                        tooltips: {
                            callbacks: {
                                label: function (tooltipItems, data) {
                                    return ' ' + tooltipItems.yLabel + ' %';
                                }
                            }
                        },

                        scales: {
                            yAxes: [{
                                stacked: true,
                                ticks: {
                                    min: 0,
                                    max: 100,
                                    fontSize: 20,
                                    fontColor: 'rgba(255,255,255,0.5)',
                                    fontFamily: 'Gilroy'
                                },
                                gridLines: {
                                    color: 'rgba(255,255,255,0.15)',
                                    zeroLineColor: 'rgba(255,255,255,1)',
                                }
                            }],
                            xAxes: [{
                                stacked: true,
                                ticks: {
                                    fontSize: 20,
                                    fontColor: '#ffffff',
                                    fontFamily: 'Gilroy'
                                },
                                gridLines: {
                                    color: 'rgba(255,255,255,0)',
                                    zeroLineColor: 'rgba(255,255,255,1)',
                                },
                            }]
                        },
                        legend: {
                            position: 'right',
                            labels: {
                                fontSize: 22,
                                fontColor: 'rgba(255,255,255,1)',
                                fontFamily: 'Gilroy'
                            }
                        }
                    },
                    data: {
                        labels: this.datas.years,
                        datasets: [
                            {
                                label: 'Pas de mobile',
                                data: this.datas.noMobile,
                                backgroundColor: 'rgba(255,224,0,1)',
                                hoverBackgroundColor: 'rgba(255,224,0,0.8)',
                            },
                            {
                                label: 'Mobile classique',
                                data: this.datas.phone,
                                backgroundColor: 'rgba(64,255,125,1)',
                                hoverBackgroundColor: 'rgba(64,255,125,0.8)',
                            },
                            {
                                label: 'Smartphone',
                                data: this.datas.smartphone,
                                backgroundColor: 'rgba(0,246,255,1)',
                                hoverBackgroundColor: 'rgba(0,246,255,0.8)',
                            }
                        ]
                    }
                });
            }
        },

        init: function () {
            this.ctx = document.getElementById('dataVizUser').getContext('2d');

            this.isBuilded = false;

            this.load();
        },

        load: function () {
            this.req = new XMLHttpRequest();
            this.req.open('GET', 'data/user.json', true);
            this.req.addEventListener('readystatechange', this.loaded.bind(this));
            this.req.send();
        },

        loaded: function () {
            if (this.req.readyState == 4) {
                if (this.req.status == 200 || this.req.status == 304) {
                    this.datas = JSON.parse(this.req.responseText);
                }
            }
        }

    };

    return Graph;
})();


userChart.init();