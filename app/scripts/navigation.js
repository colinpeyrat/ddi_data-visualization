/*
 * Navigation module 
 **/
var navigation = (function () {

    var SlideItem = function (id, graph, isHome) {
        this.el = null;
        this.graph = null;
        this.isVisible = null;
        this.init(id, graph, isHome);
    };

    SlideItem.prototype.init = function (id, graph, isHome) {
        this.isVisible = false;
        this.el = document.querySelector(id);
        this.graph = graph;
        this.isHome = isHome || false;
    };

    SlideItem.prototype.graphIn = function () {
        this.graph.buildChart();
    };

    SlideItem.prototype.graphOut = function () {
    };

    var homeSlide = new SlideItem('#slideHome', null, true);
    var slide1 = new SlideItem('#slide1', ageChart);


    slide1.graphIn = function () {
        var firstItem = this.graph.navigation.$item[0].querySelector('a');
        console.log(firstItem);
        this.graph.navigation.toggleNav(firstItem);
        this.graph.render();
    };

    slide1.graphOut = function () {
    };

    var slide2 = new SlideItem('#slide2', accessibilityChart);
    var slide4 = new SlideItem('#slide4', userChart);

    var Slide = {
        el: null,
        slides: [
            homeSlide, slide1, slide2, slide4
        ],
        currentSlide: null,
        switchWait: false,

        initListener: function () {
            $(document).on('mousewheel', this.verticalNav.bind(this));
            $(document).on('keyup', this.verticalNav.bind(this));
            $('.home-scroll__link', '#homeScroll').on('click', this.homeScroll.bind(this));
        },

        homeScroll: function () {
            this.nextSlide(true);
        },

        init: function () {
            this.initListener();
            this.el = document.querySelector('#slides');

            /* Hide all slides and play the hide animation of each slide */
            this.hideAllSlides();

            this.displayFirstSlide();
        },

        displayFirstSlide: function () {
            this.currentSlide = 0;
            var firstSlides = this.slides[0].el;

            var slideTitle = firstSlides.querySelectorAll('.home-slide__title')[0];
            var slideTitleSmall = firstSlides.querySelectorAll('.home-slide__title')[1];
            var slideSubTitle = firstSlides.querySelector('.home-slide__subtitle');
            var authors = firstSlides.querySelector('.authors');
            var scroll = firstSlides.querySelector('#homeScroll');
            var phone = firstSlides.querySelector('.background-phone');

            var authorLogos = authors.querySelectorAll('.author__logo');
            var authorName = authors.querySelectorAll('.author__name');


            TweenMax.set([slideTitleSmall, authorLogos, authorName, scroll], {autoAlpha: 0});
            TweenMax.set(scroll, {y: 20});
            TweenMax.set([slideTitle, slideTitleSmall], {y: 80});
            TweenMax.set(slideSubTitle, {y: 35});

            var tlHs = new TimelineMax();

            /* fake object to tween */
            var x = {x: 0};

            tlHs.add(TweenMax.to(x, 0.5, {
                    x: 100,
                    onComplete: function () {
                        phone.classList.add('is-drawn');
                    }
                })
            );

            tlHs.add(
                TweenMax.staggerFromTo([slideTitle, slideTitleSmall], 0.75, {
                    y: '+=40'
                }, {
                    y: 0,
                    autoAlpha: 1,
                    delay: 0.7,
                    ease: Quad.easeOut,
                }, 0.45)
            );

            tlHs.add(TweenMax.to(slideSubTitle, 0.5, {
                y: 0,
                ease: Quad.easeOut
            }));

            tlHs.add(TweenMax.staggerFromTo([authorLogos[0], authorName[0]], 0.4, {
                    y: 50
                }, {
                    y: 0,
                    autoAlpha: 1,
                    ease: Quad.easeOut
                }, 0.05)
            );

            tlHs.add(TweenMax.staggerFromTo([authorLogos[1], authorName[1]], 0.4, {
                    y: 50
                }, {
                    y: 0,
                    autoAlpha: 1,
                    ease: Quad.easeOut
                }, 0.05)
            );

            tlHs.add(TweenMax.to(scroll, 0.4, {
                    y: 0,
                autoAlpha: 1,
                ease: Quad.easeOut,
                })
            );


            /* Show first slide */
            TweenMax.set(firstSlides, {zIndex: 1, opacity: 1});
        },


        verticalNav: function (e) {
            var down = null;

            if (e.type == 'mousewheel') {
                down = e.originalEvent.wheelDelta < 0;
            } else if (e.type == 'keyup') {
                if (e.which == 40)
                    down = true;
                else if (e.which == 38)
                    down = false;
            } else if (e.type == 'swipe') {
                if (e.direction == 'up')
                    down = true;
                else if (e.direction == 'down')
                    down = false;
            } else if (e.type == 'arrow') {
                down = e.direction == 'up';
            }


            if (!this.switchWait) {
                /*  On descend */
                if (down) {
                    this.nextSlide(true)
                } else {  // On monte
                    this.nextSlide(false);
                }
            }
        },

        nextSlide: function (next) {
            var nextSlide = null;
            var activeSlide = this.slides[this.currentSlide];

            if (next) {
                if (this.currentSlide + 1 < this.slides.length) {
                    this.currentSlide++;
                    nextSlide = this.slides[this.currentSlide];
                }
            } else {
                if (this.currentSlide - 1 >= 0) {
                    this.currentSlide--;
                    nextSlide = this.slides[this.currentSlide];
                }
            }

            if (nextSlide !== null) {
                this.switchWait = true;
                activeSlide.el.classList.remove('is-active');
                this.switchSlide(nextSlide);
            }
        },

        hideAllSlides: function () {
            var slide;
            for (var i = 0; i < this.slides.length; i++) {
                slide = this.slides[i];
                TweenMax.set(slide.el, {opacity: 0, zIndex: -1});
                if (!slide.isHome) {
                    slide.graphOut();
                }
            }
        },

        switchSlide: function (slide) {

            var header = slide.el.querySelector('.slide-header__header');
            var title = slide.el.querySelector('.slide-header__title');
            var subtitle = slide.el.querySelector('.slide-header__subtitle');

            this.hideAllSlides();

            TweenMax.set(slide.el, {zIndex: 1});

            var tlShow = new TimelineMax({
                onComplete: function () {
                    this.switchWait = false;
                    slide.isVisible = true;
                },
                onCompleteScope: this
            });

            tlShow.fromTo(slide.el, 0.25, {
                alpha: 0
            }, {
                alpha: 1,
                ease: Quad.easeOut,
                force3D: true
            });

            if (!slide.isHome && !slide.isVisible) {
                tlShow.staggerFromTo([header, title, subtitle], 0.5, {
                    alpha: 0,
                    y: -25
                }, {
                    alpha: 1,
                    y: 0,
                    ease: Quad.easeOut,
                    force3D: true
                }, 0.1);

                tlShow.add(function () {
                    slide.graphIn();
                });
            }


            /* Delay beetween slides */
            var x = {x: 0};
            tlShow.to(x, 1.25, {x: 200});

            slide.el.classList.add('is-active');
        }
    };

    return Slide;
})();

navigation.init();

console.log(navigation);