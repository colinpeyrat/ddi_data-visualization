var accessibilityChart = (function () {
    var Graph = {
        ctx: null,
        graph: null,
        gradient1: null,
        gradient2: null,
        datas: null,
        isBuilded: null,

        buildChart: function () {

            if (!this.isBuilded) {
                this.isBuilded = true;
                this.chart = new Chart(this.ctx, {

                    type: 'line',
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        scaleOverride: true,
                        scaleStartValue: 0,

                        tooltips: {
                            callbacks: {
                                label: function (tooltipItems, data) {
                                    return ' ' + tooltipItems.yLabel + ' %';
                                }
                            }
                        },

                        scales: {
                            yAxes: [{
                                ticks: {
                                    min: 0,
                                    max: 100,
                                    fontSize: 20,
                                    fontColor: 'rgba(255,255,255,0.5)',
                                    fontFamily: 'Gilroy'
                                },
                                gridLines: {
                                    color: 'rgba(255,255,255,0.15)',
                                    zeroLineColor: 'rgba(255,255,255,1)',
                                }
                            }],
                            xAxes: [{
                                ticks: {
                                    fontSize: 20,
                                    fontColor: '#ffffff',
                                    fontFamily: 'Gilroy'
                                },
                                gridLines: {
                                    color: 'rgba(255,255,255,0)',
                                    zeroLineColor: 'rgba(255,255,255,1)',
                                },
                            }]
                        },

                        legend: {
                            position: 'right',
                            labels: {
                                fontSize: 22,
                                fontColor: 'rgba(255,255,255,1)',
                                fontFamily: 'Gilroy'
                            }
                        }
                    },
                    data: {
                        labels: this.datas.years,
                        datasets: [
                            {
                                label: 'Mobiles standards',
                                data: this.datas.mobile,
                                fill: false,
                                borderColor: this.gradient2,
                                pointHoverRadius: 6,
                                pointBorderColor: 'rgba(220,220,220,0)',
                                pointBackgroundColor: 'rgba(220,220,220,1)',
                                pointRadius: 6

                            },
                            {
                                label: 'Smartphones',
                                data: this.datas.smartphone,
                                fill: false,
                                borderColor: this.gradient1,
                                pointHoverRadius: 6,
                                pointBorderColor: 'rgba(220,220,220,0)',
                                pointBackgroundColor: 'rgba(220,220,220,1)',
                                pointRadius: 6
                            }
                        ]
                    }
                });
            }
        },

        init: function () {
            this.ctx = document.getElementById('dataVizAccessibility').getContext('2d');

            this.isBuilded = false;

            this.gradient1 = this.ctx.createLinearGradient(0, 0, 0, 400);
            this.gradient2 = this.ctx.createLinearGradient(0, 0, 0, 400);

            this.gradient1.addColorStop(0, 'rgba(252,219,64,1)');
            this.gradient1.addColorStop(1, 'rgba(1,255,159,1)');

            this.gradient2.addColorStop(0, 'rgba(42,163,255,1)');
            this.gradient2.addColorStop(1, 'rgba(29,255,250,1)');


            this.load();
        },

        load: function () {
            this.req = new XMLHttpRequest();
            this.req.open('GET', 'data/accessibility.json', true);
            this.req.addEventListener('readystatechange', this.loaded.bind(this));
            this.req.send();
        },

        loaded: function () {
            if (this.req.readyState == 4) {
                if (this.req.status == 200 || this.req.status == 304) {
                    this.datas = JSON.parse(this.req.responseText);
                }
            }
        }

    };

    return Graph;
})();


accessibilityChart.init();